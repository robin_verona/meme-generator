const Header = () => {
    return (
        <header className="header">
            <h3 className="header--title">Meme Generator</h3>
            <div className="header--project">React Course - Project 3</div>
        </header>
    )
}

export default Header