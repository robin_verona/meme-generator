import memesData from '../data/memes'
import { useState } from 'react';
const Meme = () => {
    const [meme, setMeme] = useState({
        topText: "", 
        bottomText: "",
        randomImage: "https://i.imgflip.com/1ihzfe.jpg"
    })

    const [allMemeImages, setAllMemeImage] = useState(memesData)

    function getMemeImage() {
        const memesArray = allMemeImages.data.memes
        const randomNumber = Math.floor(Math.random() * memesArray.length)
        setMeme(prevMeme => {
            return {
                ...prevMeme, 
                randomImage: memesArray[randomNumber].url
            }
        })
        
    }
    return (
        <main>
            <div className="form">
                <input className="form--input" placeholder="top text" type="text" name="" id="" />
                <input className="form--input" placeholder="bottom text" type="text" name="" id="" />
                <button onClick={getMemeImage} className="form--button">Get a new meme image 🖼️</button>
            </div>
            <img src={meme.randomImage} alt="" className="meme--image" />
        </main>
    )
}

export default Meme