# Meme generator (WIP)

Meme generator made for the Learn React course by Scrimba

https://scrimba.com/learn/learnreact

## Getting started

After cloning the project, install the dependencies `npm install`

Then start the development server `npm run dev`
